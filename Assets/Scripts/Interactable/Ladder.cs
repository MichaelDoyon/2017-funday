﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ladder : MonoBehaviour
{
    private void Start()                                                      
    {                                                                         
        gameObject.tag = "Ladder";                                            
    }

    private void OnTriggerEnter(Collider col)
    {
        BaseMotor bm = col.GetComponent<BaseMotor>();
        if (bm != null)
        {
            bm.ChangeState("ClimbingLadder",-transform.parent.forward);
        }
    }

    private void OnTriggerExit(Collider col)
    {
        BaseMotor bm = col.GetComponent<BaseMotor>();
        if (bm != null)
        {
            bm.ChangeState("Falling");
        }
    }
}
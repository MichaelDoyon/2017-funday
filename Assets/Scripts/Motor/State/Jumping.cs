﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : BaseState
{
    public float jumpForce = 14;
    public float gravity = 21.0f;
    public float moveSpeed = 2.5f;

    public override void Construct()
    {
        base.Construct();
        motor.VerticalVelocity = jumpForce;
    }

    public override Vector3 ProcessMotion(Vector3 input)
    {
        MotorHelper.KillVector(ref input, motor.WallVector);
        MotorHelper.ApplySpeed(ref input, moveSpeed);
        MotorHelper.ApplyGravity(ref input, ref motor.VerticalVelocity, gravity, 30.0f);

        return base.ProcessMotion(input);
    }

    public override void Transition()
    {
        if (motor.VerticalVelocity < 0)
            motor.ChangeState("Falling");
    }
}

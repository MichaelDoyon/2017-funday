﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseState : MonoBehaviour
{
    protected BaseMotor motor;

    private void Awake()
    {
        motor = GetComponent<BaseMotor>();
    }

    public virtual void Construct()
    {

    }

    public virtual void Construct(params object[] p)
    {

    }

    public virtual void Destruct()
    {

    }

    public virtual void Transition()
    {

    }

    public virtual Vector3 ProcessMotion(Vector3 input)
    {
        return input;
    }

    public virtual Quaternion ProcessRotation(Vector3 input)
    {
        return Quaternion.identity;
    }
}
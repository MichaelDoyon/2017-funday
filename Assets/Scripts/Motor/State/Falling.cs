﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Falling : BaseState
{
    private float moveSpeed = 2.5f;

    public override Vector3 ProcessMotion(Vector3 input)
    {
        MotorHelper.KillVector(ref input, motor.WallVector);
        MotorHelper.ApplySpeed(ref input, moveSpeed);
        MotorHelper.ApplyGravity(ref input, ref motor.VerticalVelocity, motor.Gravity, motor.TerminalVelocity);

        return input;
    }

    public override void Transition()
    {
        if (motor.Grounded())
            motor.ChangeState("Walking");
    }
}

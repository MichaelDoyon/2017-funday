﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClimbingLadder : BaseState
{
    private float climbSpeed = 3.0f;
    private Vector3 ladderNormal;

    // Params = Vector3 (Ladder normal)
    public override void Construct(params object[] p)
    {
        base.Construct();
        motor.VerticalVelocity = 0;
        ladderNormal = (Vector3)p[0];
    }

    public override void Destruct()
    {
        motor.MoveVector = ladderNormal * 20.0f;
    }

    public override Vector3 ProcessMotion(Vector3 input)
    {
        input = Vector3.up * input.z * climbSpeed;
        input -= ladderNormal;

        return base.ProcessMotion(input);
    }

    public override void Transition()
    {
        if (motor.Grounded() && motor.InputVector.z < 0)
            motor.ChangeState("Walking");

        if (Input.GetKeyDown(KeyCode.Space))
            motor.ChangeState("Falling");
    }
}

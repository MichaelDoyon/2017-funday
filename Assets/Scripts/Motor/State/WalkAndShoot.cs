﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkAndShoot : BaseState
{
    private float walkSpeed = 2.0f;
    private CameraBaseState camState;

    public override void Construct()
    {
        base.Construct();

        camState = new WalkAndShootCamera();

        if (motor.GetCamMotor())
            motor.GetCamMotor().AddState(camState, 3);
    }

    public override void Destruct()
    {
        base.Destruct();

        // Get out of shooting camera state
        if (motor.GetCamMotor())
            motor.GetCamMotor().RemoveState(camState);
    }

    public override Vector3 ProcessMotion(Vector3 input)
    {
        MotorHelper.KillVector(ref input, motor.WallVector);
        MotorHelper.FollowVector(ref input, motor.SlopeNormal);
        MotorHelper.ApplySpeed(ref input, walkSpeed);

        return input + Vector3.down;
    }

    public override void Transition()
    {
        base.Transition();

        if (!motor.Grounded())
            motor.ChangeState("Falling");

        if (Input.GetKeyDown(KeyCode.Space))
            motor.ChangeState("Jumping");

        if (Input.GetMouseButtonUp(1))
            motor.ChangeState("Walking");
    }
}
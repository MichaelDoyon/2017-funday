﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walking : BaseState
{
    private float walkSpeed = 5.0f;

    public override void Construct()
    {
        base.Construct();
        motor.VerticalVelocity = 0.0f;
    }

    public override Vector3 ProcessMotion(Vector3 input)
    {
        MotorHelper.KillVector(ref input, motor.WallVector);
        MotorHelper.FollowVector(ref input, motor.SlopeNormal);
        MotorHelper.ApplySpeed(ref input, walkSpeed);

        return input + Vector3.down;
    }

    public override void Transition()
    {
        base.Transition();

        if (!motor.Grounded())
            motor.ChangeState("Falling");

        if (Input.GetKeyDown(KeyCode.Space))
            motor.ChangeState("Jumping");

        if (Input.GetMouseButtonDown(1))
            motor.ChangeState("WalkAndShoot");
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMotor : BaseMotor
{
    private Transform cameraContainer;
    private CameraMotor camMotor;

    public Text stateText, inputText, wallText, slopeText, moveText;

    protected override void Start()
    {
        base.Start();
        cameraContainer = Camera.main.transform;
        camMotor = FindObjectOfType<CameraMotor>();
    }

    private void UpdateDebugPanel()
    {
        stateText.text = "Current State : " + state.ToString();
        moveText.text = "Move Vector : " +  MoveVector.ToString();
        inputText.text = "Input Vector : " + InputVector.ToString();
        wallText.text = "Wall Vector : " + WallVector.ToString();
        slopeText.text = "Slope Vector : " + SlopeNormal.ToString();
    }

    protected override void UpdateMotor()
    {
        // This will display us some dank infos
        UpdateDebugPanel();

        // Start by getting the players input
        Vector3 input = PoolInput();
        InputVector = input;

        // Rotate the InputVector with the camera's direction
        MotorHelper.RotateWithView(ref input, cameraContainer);

        // Transfers inputs to the movevector before parsing it
        MoveVector = input;

        // Process our input vector to get a delta position
        MoveVector = state.ProcessMotion(MoveVector);

        // Process our input vector to get a rotation 
        RotationQuaternion = state.ProcessRotation(InputVector);

        // Look if the state need to transition
        state.Transition();

        // Moves!
        Move();
        Rotate();
    }

    private Vector3 PoolInput()
    {
        Vector3 dir = Vector3.zero;

        dir.x = Input.GetAxis("Horizontal");
        dir.z = Input.GetAxis("Vertical");

        if (dir.sqrMagnitude > 1)
            dir.Normalize();

        return dir;
    }

    public override CameraMotor GetCamMotor()
    {
        return camMotor;
    }
}
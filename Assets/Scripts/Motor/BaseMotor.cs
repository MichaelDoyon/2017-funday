﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseMotor : MonoBehaviour
{
    private const float DISTANCE_GROUNDED = 0.5f;
    private const float INNER_OFFSET_GROUNDED = 0.05f;
    private const float SLOPE_TRESHOLD = 0.55f;

    public float TerminalVelocity { get { return 15.0f; } }
    public float Gravity { get { return 25.0f; } }

    protected CharacterController controller;
    protected BaseState state;

    public Vector3 WallVector { set; get; }
    public Vector3 SlopeNormal { set; get; }
    public Vector3 MoveVector { set; get; }
    public Quaternion RotationQuaternion { set; get; }
    public Vector3 InputVector { set; get; }
    public CollisionFlags ColFlags { set; get; }
    [HideInInspector]
    public float VerticalVelocity;

    protected virtual void Start()
    {
        controller = GetComponent<CharacterController>();

        // Initiate the state of our player to Walking State
        state = GetComponent<Walking>();
        state.Construct();
    }

    private void Update()
    {
        UpdateMotor();
    }

    protected virtual void UpdateMotor()
    {
        // Process our input vector to get a delta position
        MoveVector = state.ProcessMotion(InputVector);

        // Process our input vector to get a rotation 
        RotationQuaternion = state.ProcessRotation(InputVector);

        // Look if the state need to transition
        Move();
        Rotate();
    }

    protected virtual void Move()
    {
        ColFlags = controller.Move(MoveVector * Time.deltaTime);
        WallVector = (((ColFlags & CollisionFlags.Sides) != 0) ? WallVector : Vector3.zero);
    }

    protected virtual void Rotate()
    {
        transform.rotation = RotationQuaternion;
    }

    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        if (hit.normal.y > -0.2f && hit.normal.y < 0.2f)
        {
            WallVector = hit.normal;
            Debug.DrawRay(hit.point, hit.normal, Color.cyan, 3.0f);
        }
    }

    public virtual bool Grounded()
    {
        float yRay = controller.bounds.center.y - (controller.height * 0.5f) + 0.3f;
        RaycastHit hit;

        // Mid
        if (Physics.Raycast(new Vector3(controller.bounds.center.x, yRay, controller.bounds.center.z), -Vector3.up, out hit, DISTANCE_GROUNDED))
        {
            SlopeNormal = hit.normal;
            return (SlopeNormal.y > SLOPE_TRESHOLD) ? true : false;
        }

        // Front-Right
        if (Physics.Raycast(new Vector3(controller.bounds.center.x + (controller.bounds.extents.x - INNER_OFFSET_GROUNDED), yRay, controller.bounds.center.z + (controller.bounds.extents.z - INNER_OFFSET_GROUNDED)), -Vector3.up, out hit, DISTANCE_GROUNDED))
        {
            SlopeNormal = hit.normal;
            return (SlopeNormal.y > SLOPE_TRESHOLD) ? true : false;
        }

        // Front-Left
        if (Physics.Raycast(new Vector3(controller.bounds.center.x - (controller.bounds.extents.x - INNER_OFFSET_GROUNDED), yRay, controller.bounds.center.z + (controller.bounds.extents.z - INNER_OFFSET_GROUNDED)), -Vector3.up, out hit, DISTANCE_GROUNDED))
        {
            SlopeNormal = hit.normal;
            return (SlopeNormal.y > SLOPE_TRESHOLD) ? true : false;
        }
        // Back Right
        if (Physics.Raycast(new Vector3(controller.bounds.center.x + (controller.bounds.extents.x - INNER_OFFSET_GROUNDED), yRay, controller.bounds.center.z - (controller.bounds.extents.z - INNER_OFFSET_GROUNDED)), -Vector3.up, out hit, DISTANCE_GROUNDED))
        {
            SlopeNormal = hit.normal;
            return (SlopeNormal.y > SLOPE_TRESHOLD) ? true : false;
        }
        // Back Left
        if (Physics.Raycast(new Vector3(controller.bounds.center.x - (controller.bounds.extents.x - INNER_OFFSET_GROUNDED), yRay, controller.bounds.center.z - (controller.bounds.extents.z - INNER_OFFSET_GROUNDED)), -Vector3.up, out hit, DISTANCE_GROUNDED))
        {
            SlopeNormal = hit.normal;
            return (SlopeNormal.y > SLOPE_TRESHOLD) ? true : false;
        }

        return false;
    }

    public virtual void ChangeState(string stateName)
    {
        state.Destruct();
        state = GetComponent(stateName) as BaseState;
        state.Construct();
    }

    public virtual void ChangeState(string stateName,params object[] p)
    {
        state.Destruct();
        state = GetComponent(stateName) as BaseState;
        state.Construct(p);
    }

    public virtual CameraMotor GetCamMotor()
    {
        return null;
    }
}

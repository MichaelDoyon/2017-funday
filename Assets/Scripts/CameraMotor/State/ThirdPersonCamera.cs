﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThirdPersonCamera : CameraBaseState
{
    private float distance = 10.0f;
    private float currentX, currentY;

    private float sensivityX = 4.0f;
    private float sensivityY = 1.0f;

    public override Vector3 ProcessMotion(Vector3 input)
    {
        Vector3 dir = new Vector3(0, 0, -distance);

        currentX += input.x * sensivityX;
        currentY += input.y * sensivityY;
        currentY = Mathf.Clamp(currentY, 0.0f, 50.0f);

        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);

        return motor.playerTransform.position + rotation * dir;
    }

    public override Quaternion ProcessRotation(Vector3 input)
    {
        motor.camContainer.LookAt(motor.playerTransform);
        return motor.camContainer.rotation;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CameraBaseState
{
    protected CameraMotor motor;
    protected int priority = 0;

    public virtual void Construct(CameraMotor motor)
    {
        this.motor = motor;
    }

    public virtual void Construct(params object[] p)
    {

    }

    public virtual void Destruct()
    {

    }

    public virtual void Transition()
    {

    }

    public virtual Vector3 ProcessMotion(Vector3 input)
    {
        return input;
    }

    public virtual Quaternion ProcessRotation(Vector3 input)
    {
        return Quaternion.identity;
    }

    public int Priority { get { return priority; } }
}
﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraMotor : MonoBehaviour
{
    private SortedDictionary<int,CameraBaseState> activeStates = new SortedDictionary<int, CameraBaseState>();
    private CameraBaseState state;

    public Camera cam;
    public Transform camContainer;
    public Transform playerTransform;

    public Vector3 CameraInput { set; get; }

    public Text stateText, inputText;

    private void Awake()
    {
        var thirdPersonState = new ThirdPersonCamera();
        thirdPersonState.Construct(this);
        state = thirdPersonState;
        AddState(thirdPersonState, 0);
    }

    private void Update()
    {
        UpdateDebugPanel();
        CameraInput = PoolInput();
    }

    private void UpdateDebugPanel()
    {
        stateText.text = "Camera State : " + state.ToString();
    }

    private void LateUpdate()
    {
        // Move the camera based on the active state
        transform.position = state.ProcessMotion(CameraInput);
        transform.rotation = state.ProcessRotation(CameraInput);
    }

    private void SortStates()
    {
        CameraBaseState current = state;
        if (current == null)
            return;

        state = activeStates.Values.Last();

        if (current == state)
            return;

        current.Destruct();
        state.Construct(this);
    }

    public virtual void AddState(CameraBaseState st,int priority)
    {
        activeStates.Add(priority, st);
        SortStates();
    }

    public virtual void RemoveState(CameraBaseState st)
    {
        activeStates.Remove(activeStates.First(x => x.Value == st).Key);
        SortStates();

        Debug.Log("States : ");
        foreach (CameraBaseState s in activeStates.Values)
        {
            Debug.Log(s);
        }
    }

    private Vector3 PoolInput()
    {
        Vector3 mouseAxis = Vector3.zero;

        mouseAxis.x = Input.GetAxis("Mouse X");
        mouseAxis.y = Input.GetAxis("Mouse Y");

        return mouseAxis;
    }
}